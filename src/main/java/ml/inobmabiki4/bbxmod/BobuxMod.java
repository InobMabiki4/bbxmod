package ml.inobmabiki4.bbxmod;

import ml.inobmabiki4.bbxmod.block.Blocks;
import ml.inobmabiki4.bbxmod.item.Items;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;

@Mod("bbxmod")
public class BobuxMod
{
    public static final String MODID = "bbxmod";

    public BobuxMod() {
        Blocks.register();
        Items.register();

        MinecraftForge.EVENT_BUS.register(this);
    }
}
