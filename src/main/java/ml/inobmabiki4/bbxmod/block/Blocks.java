package ml.inobmabiki4.bbxmod.block;

import ml.inobmabiki4.bbxmod.BobuxMod;

import net.minecraft.block.Block;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class Blocks {
    private static final DeferredRegister<Block> REGISTRY = DeferredRegister.create(ForgeRegistries.BLOCKS, BobuxMod.MODID);
    public static void register() { REGISTRY.register(FMLJavaModLoadingContext.get().getModEventBus()); }

    public static final RegistryObject<Block> BOBUX_MINER = REGISTRY.register("bobux_miner", BobuxMinerBlock::new);
    public static final RegistryObject<Block> BOBUX_BLOCK_001 = REGISTRY.register("one_bobux_block", BobuxBlock::new);
    public static final RegistryObject<Block> BOBUX_BLOCK_002 = REGISTRY.register("two_bobux_block", BobuxBlock::new);
    public static final RegistryObject<Block> BOBUX_BLOCK_005 = REGISTRY.register("five_bobux_block", BobuxBlock::new);
    public static final RegistryObject<Block> BOBUX_BLOCK_010 = REGISTRY.register("ten_bobux_block", BobuxBlock::new);
    public static final RegistryObject<Block> BOBUX_BLOCK_020 = REGISTRY.register("twenty_bobux_block", BobuxBlock::new);
    public static final RegistryObject<Block> BOBUX_BLOCK_050 = REGISTRY.register("fifty_bobux_block", BobuxBlock::new);
    public static final RegistryObject<Block> BOBUX_BLOCK_100 = REGISTRY.register("hundred_bobux_block", BobuxBlock::new);
}
