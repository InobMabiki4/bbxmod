package ml.inobmabiki4.bbxmod.block;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class BobuxBlock extends Block {

    public BobuxBlock() {
        super(Block.Properties
                .create(Material.WOOL).sound(SoundType.SNOW)
                .hardnessAndResistance(0.8F)
        );
    }
}
