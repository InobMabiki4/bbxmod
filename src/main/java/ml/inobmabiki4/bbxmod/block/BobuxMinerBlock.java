package ml.inobmabiki4.bbxmod.block;

import ml.inobmabiki4.bbxmod.item.Items;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.HorizontalBlock;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.ItemStack;
import net.minecraft.state.StateContainer;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.Mirror;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.World;
import net.minecraftforge.common.ToolType;

import java.util.Random;

public class BobuxMinerBlock extends HorizontalBlock {
    private static final Random random = new Random();

    public BobuxMinerBlock() {
        super(Block.Properties
                .create(Material.IRON).sound(SoundType.ANVIL)
                .harvestTool(ToolType.PICKAXE).harvestLevel(0)
                .hardnessAndResistance(3.5F)
        );
        this.setDefaultState(this.stateContainer.getBaseState().with(HORIZONTAL_FACING, Direction.NORTH));
    }

    protected ItemStack generateBobux() {
        int rand = random.nextInt(100) + 1;
        if (rand > 99) return new ItemStack(Items.BOBUX_NOTE_100.get());  // .01 chance
        if (rand > 95) return new ItemStack(Items.BOBUX_NOTE_050.get());  // .04 chance
        if (rand > 90) return new ItemStack(Items.BOBUX_NOTE_020.get());  // .05 chance
        if (rand > 80) return new ItemStack(Items.BOBUX_NOTE_010.get());  // .10 chance
        if (rand > 65) return new ItemStack(Items.BOBUX_NOTE_005.get());  // .15 chance
        if (rand > 50) return new ItemStack(Items.BOBUX_NOTE_002.get());  // .15 chance
        else           return new ItemStack(Items.BOBUX_NOTE_001.get());  // .50 chance
    }

    protected ItemEntity dropItemsOnTop(World world, BlockPos pos, ItemStack items)
    {
        ItemEntity itemEntity = new ItemEntity(world, pos.getX() + .5, pos.getY() + 1, pos.getZ() +.5, items);
        world.addEntity(itemEntity);
        return itemEntity;
    }

    public BlockState getStateForPlacement(BlockItemUseContext context) {
        return this.getDefaultState().with(HORIZONTAL_FACING, context.getPlacementHorizontalFacing().getOpposite());
    }

    public BlockState rotate(BlockState state, Rotation rot) {
        return state.with(HORIZONTAL_FACING, rot.rotate(state.get(HORIZONTAL_FACING)));
    }

    public BlockState mirror(BlockState state, Mirror mirrorIn) {
        return state.rotate(mirrorIn.toRotation(state.get(HORIZONTAL_FACING)));
    }

    public ActionResultType onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
        dropItemsOnTop(worldIn, pos, generateBobux());
        return ActionResultType.SUCCESS;
    }

    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        builder.add(HORIZONTAL_FACING);
    }
}
