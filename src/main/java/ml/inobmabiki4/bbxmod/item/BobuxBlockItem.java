package ml.inobmabiki4.bbxmod.item;

import net.minecraft.block.Block;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;

import java.util.List;

public class BobuxBlockItem extends BlockItem {
    private final int bobuxAmount;

    public BobuxBlockItem(Block blockIn, int bobuxAmount) {
        super(blockIn, new Item.Properties().group(ItemGroup.DECORATIONS));
        this.bobuxAmount = bobuxAmount;
    }

    @Override
    public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("" + TextFormatting.GRAY + "= " + bobuxAmount * stack.getCount() + " Bobux"));
    }
}
