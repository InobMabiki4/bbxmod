package ml.inobmabiki4.bbxmod.item;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;

import java.util.List;

public class BobuxItem extends Item {
    private final int bobuxAmount;

    public BobuxItem(int bobuxAmount) {
        super(new Item.Properties().group(ItemGroup.MISC));
        this.bobuxAmount = bobuxAmount;
    }

    @Override
    public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("" + TextFormatting.GRAY + "= " + bobuxAmount * stack.getCount() + " Bobux"));
    }
}
