package ml.inobmabiki4.bbxmod.item;

import ml.inobmabiki4.bbxmod.BobuxMod;
import ml.inobmabiki4.bbxmod.block.Blocks;

import net.minecraft.item.Item;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class Items {
    private static final DeferredRegister<Item> REGISTRY = DeferredRegister.create(ForgeRegistries.ITEMS, BobuxMod.MODID);
    public static void register() { REGISTRY.register(FMLJavaModLoadingContext.get().getModEventBus()); }

    public static final RegistryObject<Item> BOBUX_NOTE_001 = REGISTRY.register("one_bobux_note", () -> new BobuxItem(1));
    public static final RegistryObject<Item> BOBUX_NOTE_002 = REGISTRY.register("two_bobux_note", () -> new BobuxItem(2));
    public static final RegistryObject<Item> BOBUX_NOTE_005 = REGISTRY.register("five_bobux_note", () -> new BobuxItem(5));
    public static final RegistryObject<Item> BOBUX_NOTE_010 = REGISTRY.register("ten_bobux_note", () -> new BobuxItem(10));
    public static final RegistryObject<Item> BOBUX_NOTE_020 = REGISTRY.register("twenty_bobux_note", () -> new BobuxItem(20));
    public static final RegistryObject<Item> BOBUX_NOTE_050 = REGISTRY.register("fifty_bobux_note", () -> new BobuxItem(50));
    public static final RegistryObject<Item> BOBUX_NOTE_100 = REGISTRY.register("hundred_bobux_note", () -> new BobuxItem(100));

    public static final RegistryObject<Item> BOBUX_BUNCH_001 = REGISTRY.register("one_bobux_bunch", () -> new BobuxItem(9));
    public static final RegistryObject<Item> BOBUX_BUNCH_002 = REGISTRY.register("two_bobux_bunch", () -> new BobuxItem(18));
    public static final RegistryObject<Item> BOBUX_BUNCH_005 = REGISTRY.register("five_bobux_bunch", () -> new BobuxItem(45));
    public static final RegistryObject<Item> BOBUX_BUNCH_010 = REGISTRY.register("ten_bobux_bunch", () -> new BobuxItem(90));
    public static final RegistryObject<Item> BOBUX_BUNCH_020 = REGISTRY.register("twenty_bobux_bunch", () -> new BobuxItem(180));
    public static final RegistryObject<Item> BOBUX_BUNCH_050 = REGISTRY.register("fifty_bobux_bunch", () -> new BobuxItem(450));
    public static final RegistryObject<Item> BOBUX_BUNCH_100 = REGISTRY.register("hundred_bobux_bunch", () -> new BobuxItem(900));

    public static final RegistryObject<Item> BOBUX_BLOCK_001 = REGISTRY.register("one_bobux_block", () -> new BobuxBlockItem(Blocks.BOBUX_BLOCK_001.get(), 81));
    public static final RegistryObject<Item> BOBUX_BLOCK_002 = REGISTRY.register("two_bobux_block", () -> new BobuxBlockItem(Blocks.BOBUX_BLOCK_002.get(), 162));
    public static final RegistryObject<Item> BOBUX_BLOCK_005 = REGISTRY.register("five_bobux_block", () -> new BobuxBlockItem(Blocks.BOBUX_BLOCK_005.get(), 405));
    public static final RegistryObject<Item> BOBUX_BLOCK_010 = REGISTRY.register("ten_bobux_block", () -> new BobuxBlockItem(Blocks.BOBUX_BLOCK_010.get(), 810));
    public static final RegistryObject<Item> BOBUX_BLOCK_020 = REGISTRY.register("twenty_bobux_block", () -> new BobuxBlockItem(Blocks.BOBUX_BLOCK_020.get(), 1620));
    public static final RegistryObject<Item> BOBUX_BLOCK_050 = REGISTRY.register("fifty_bobux_block", () -> new BobuxBlockItem(Blocks.BOBUX_BLOCK_050.get(), 4050));
    public static final RegistryObject<Item> BOBUX_BLOCK_100 = REGISTRY.register("hundred_bobux_block", () -> new BobuxBlockItem(Blocks.BOBUX_BLOCK_100.get(), 8100));

    public static final RegistryObject<Item> BOBUX_MINER = REGISTRY.register("bobux_miner", () -> new BobuxMinerItem(Blocks.BOBUX_MINER.get()));
}
